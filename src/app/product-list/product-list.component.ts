import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  url = "http://127.0.0.1:8000/product";
  products = [];
  columnsToDisplay = ['id','name','description','price','action'];
  
  productForm = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    price: new FormControl(''),
   // mail: new FormControl(''),
  });
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.loadProducts();
  }

  loadProducts() {
    this.http.get(this.url).subscribe((data:any) => {
      this.products = data;
    })
  }

  deleteProduct(id:string){
    this.http.delete(this.url + '/' + id).subscribe((status: string) => {
      this.loadProducts();
    })
  }

  onSubmit() {
    this.http.post(this.url, this.productForm.value).subscribe((response: any) => {
      this.loadProducts();
      this.productForm.reset();
    });
  }
}


