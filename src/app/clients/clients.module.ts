import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from '../client/client.component';
import {MatButtonModule, MatCheckboxModule, MatTableModule, MatInputModule} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ClientComponent],
  imports: [
    CommonModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatInputModule,
    MatTableModule,
    ReactiveFormsModule
  ],
  exports:[
    ClientComponent,
  ]
})
export class ClientsModule { }
