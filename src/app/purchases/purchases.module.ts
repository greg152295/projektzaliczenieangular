import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseListComponent } from '../purchase-list/purchase-list.component';
import {MatButtonModule, MatCheckboxModule, MatTableModule} from '@angular/material';

@NgModule({
  declarations: [PurchaseListComponent],
  imports: [
    CommonModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatTableModule
  ],
  exports:[
    PurchaseListComponent
  ]
})
export class PurchasesModule { }
