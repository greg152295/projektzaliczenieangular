import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  url = "http://127.0.0.1:8000/client";
  clients = [];
  columnsToDisplay = ['id','clientName','lastname','mail','phone','action'];

  // forms
  clientForm = new FormGroup({
    name: new FormControl(''),
    lastname: new FormControl(''),
    phonenumber: new FormControl(''),
    mail: new FormControl(''),
  });
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.loadClients();
  }

  loadClients() {
    this.http.get(this.url).subscribe((data:any) => {
      this.clients = data;
    })
  }

  deleteClient(id:string){
    this.http.delete(this.url + '/' + id).subscribe((status: string) => {
      this.loadClients();
    })
  }

  onSubmit() {
    this.http.post(this.url, this.clientForm.value).subscribe((response: any) => {
      this.loadClients();
      this.clientForm.reset();
    });
  }


}
