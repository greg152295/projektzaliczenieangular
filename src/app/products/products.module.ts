import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from '../product-list/product-list.component';
import {MatButtonModule, MatCheckboxModule, MatTableModule, MatInputModule, } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProductListComponent],
  imports: [
    CommonModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatTableModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports:[
    ProductListComponent
  ]
})
export class ProductsModule { }
